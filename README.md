#Deployment Guide (13.04.2021)

## Installation

- clone git project
- Database setup
- Composer install

composer install command: composer install

## Usage

### Configure .env file

```.env file
DATABASE_URL=mysql://db_user:abc@db_password@127.0.0.1:3306/ateities?serverVersion=mariadb-10.4.11
```

#### Debug mode off

```.env file
APP_DEBUG=0;
APP_ENV=prod
```

### Configure base url for twig file

```config\packages\twig.yaml
globals:
        base_url: http://127.0.0.1:8000/    
```

### Configure for global parameters

```config\services.yaml
parameters:
     db_url: "127.0.0.1"
     db_port: "3306"
     db_user: "root"
     db_password: ""
     base_url: "http://127.0.0.1:8000"
```

### Command for cache clear

php bin/console cache:clear

bin/console cache:clear --env=prod --no-debug

### remove symfony profile(bottom toolbar, it mainly used for debugging)

```.env file
APP_DEBUG=0;
APP_ENV=prod
```

### debug mode on

```.env file
APP_DEBUG=1;
APP_ENV=dev
```

### cache clear (if facing issue with cache)

```
php bin/console make:entity --regenerate App
php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console cache:clear
```

### Project Run on local

```
php -S 127.0.0.1:8000 -t public

php bin/console make:entity --regenerate App

php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console cache:clear


```
