<?php

namespace App\Controller;

use App\Entity\Door;
use App\Form\RoutingFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DoorController extends AbstractController
{


    /**
     * @Route("/doors", name="doors")
     */
    public function index(SessionInterface $session)
    {

        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $doors = $this->getDoctrine()->getRepository(Door::class)->findBy([],
            ['id' => 'DESC']
        );

        return $this->render('door/index.html.twig', [
            'controller_name' => 'Door List',
            'doors' => $doors
        ]);
    }

    /**
     * @Route("/door_create", name="door_create")
     */
    public function index_door_create(SessionInterface $session)
    {
        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        return $this->render('door/create.html.twig', [
            'controller_name' => 'Add Door'
        ]);
    }

    /**
     * @Route("/create_door", name="create_door", methods="POST")
     */
    public function create(Request $request, SessionInterface $session)
    {
        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $id = trim($request->request->get('id'));
        $title = trim($request->request->get('title'));

        if (empty($title)) {
            $this->addFlash("warning", "Please Give Door Name");
            return $this->redirectToRoute('door_create');
        } else {
            $entityManager = $this->getDoctrine()->getManager();
            if(!empty($id)){
                $door = $entityManager
                    ->getRepository(Door::class)
                    ->find($id);


                $msg = 'Update';
            }else{
                $door = new Door();
                $door->setUserId($session->get('userID'));
                $msg = 'Created';
            }

            $door->setTitle($title);
            $door->setWidth($request->request->get('width'));
            $door->setHeight($request->request->get('height'));
            $door->setColor($request->request->get('color'));
            $door->setHandleSide($request->request->get('handle_side'));
            $entityManager->persist($door);
            $entityManager->flush();

            $this->addFlash("success", "Door Successfully ". $msg);
            return $this->redirectToRoute('doors');
        }
    }

    /**
     * @Route("/door/edit_door")
     */
    public function editDoor(SessionInterface $session, LoggerInterface $logger, Request $request)
    {
        $id = $request->query->get('door_id');

        $entityManager = $this->getDoctrine()->getManager();

        $door = $entityManager
            ->getRepository(Door::class)
            ->find($id);

        $data = [];

        $data['id'] = $id;
        $data['title'] = $door->getTitle();
        $data['width'] = $door->getWidth();
        $data['height'] = $door->getHeight();
        $data['color'] = $door->getColor();
        $data['handle_side'] = $door->getHandleSide();

        $json = json_encode($data);

        return new JsonResponse($json);

    }

    /**
     * @Route("/door-delete/{id}", name="door_delete")
     */
    public function delete(SessionInterface $session, $id)
    {
        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $door = $entityManager->getRepository(Door::class)->find($id);
        $entityManager->remove($door);
        $entityManager->flush();

        $this->addFlash("success", "Door Successfully Delete");
        return $this->redirectToRoute('doors');
    }

    /**
     * @Route("/door/check_door/{title}")
     */
    public function checkDoor(SessionInterface $session, LoggerInterface $logger, Request $request, $title)
    {
        $door = $this->getDoctrine()
            ->getRepository(Door::class)
            ->findOneBy(['title' => $title]);

        if(!empty($door)){
            $response = "exists";

        }else{
            $response = 'notexists';
        }

        return new Response(
            $response
        );

    }

}