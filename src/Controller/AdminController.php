<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\User;
use App\Form\PasswordChangeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin-profile", name="adminProfile")
     */
    public function index(Request $request, SessionInterface $session)
    {

        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $id = trim($request->request->get('id'));

        if (!empty($id)) {
            $email = trim($request->request->get('email'));
            $contact_no = trim($request->request->get('contact_no'));

            $file = $request->files->get('photo');

            $entityManager = $this->getDoctrine()->getManager();

            $admin = $entityManager
                ->getRepository(Admin::class)
                ->find($id);

            if (isset($file)) {

                //  Delete Old file
                if ($admin->getPhoto() != null) {
                    if (file_exists('uploads/img/admins/' . $admin->getPhoto())) {
                        unlink('uploads/img/admins/' . $admin->getPhoto());
                    }
                }

                $fileName = $file . '.' . $file->getClientOriginalExtension();
                if (!file_exists('uploads/img/admins')) {
                    try {
                        mkdir('uploads/img/admins', 0777, true);
                    } catch (\Exception $e) {
                    }
                }
                $file->move('uploads/img/admins', $fileName);
                $fileName = basename($fileName, ".php");

                $session->set('userPhoto', $fileName);

                $admin->setPhoto($fileName);
            }

            $admin->setEmail($email);
            $admin->setContactNo($contact_no);

            $entityManager->persist($admin);

            $user = $entityManager
                ->getRepository(User::class)
                ->findOneBy(['role_id'=>2, 'concern_person_id'=>$id]);

            $user->setEmail($email);
            $entityManager->persist($user);

            $entityManager->flush();

            $session->set('userEmail', $email);
            $session->set('ContactNo', $contact_no);

            $this->addFlash("success", "Successfully updated!");
            return $this->redirectToRoute('adminProfile');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $admin = $entityManager
            ->getRepository(Admin::class)
            ->find($session->get('UserConcernPerson'));


        return $this->render('admin/profile.html.twig', [
            'controller_name' => 'AdminController',
            'admin' => $admin
        ]);
    }

     /**
     * @Route("/change-password", name="changePassword")
     */
    public function index_student_passwordChange(Request $request, SessionInterface $session)
    {
        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $current_pass = trim($request->request->get('current_pass'));
        $password1 = trim($request->request->get('password1'));
        $confirm_pass = trim($request->request->get('confirm_pass'));

        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(
            [
                'school_id'=>$session->get('userID'),
                'concern_person_id' => $session->get('UserConcernPerson'),
                'role_id' => 2
            ]
        );

        if (empty($user)) {
            return $this->redirectToRoute('adminProfile');
        }

        if(!empty($password1) && !empty($confirm_pass) && !empty($current_pass)){

            if ($password1 != $confirm_pass){
                $this->addFlash("warning", "Passwords do not match");
                return $this->redirectToRoute('login');
            } else {

                // Check Password
                if (!password_verify($current_pass, $user->getPassword())) {
                    $this->addFlash("warning", "Wrong Password");
                    return $this->redirectToRoute('login');
                }

                $hashed_password = password_hash($password1, PASSWORD_DEFAULT);
                $user->setPassword($hashed_password);
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash("success", "Password successfully updated");

                return $this->redirectToRoute('adminProfile');
            }
        }

        return $this->render('admin/changePassword.html.twig', [
            'controller_name' => 'Change Password'
        ]);
    }
}
