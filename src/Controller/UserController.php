<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\User;
use App\Entity\Role;
use PhpParser\Node\Stmt\TryCatch;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class UserController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(SessionInterface $session)
    {
        if ($session->get('UserType') == 2) {
            return $this->redirectToRoute('orders');
        }

        return $this->render('template.html.twig', [
            'controller_name' => 'Template'
        ]);
    }


    /**
     * @Route("/temp_login", name="temp_login", methods="POST")
     */
    public function indexLogin(Request $request, SessionInterface $session, LoggerInterface $logger)
    {
        //Get School id and check

        $user_name = trim($request->request->get('username'));
        $user_password = trim($request->request->get('userpassword'));

        if (empty($user_name) || empty($user_password)) {
            $this->addFlash("warning", "Please give UserId and Password");
            return $this->redirectToRoute('login');
        }


        $_SESSION['flag'] = TRUE;

        $user = $this->getDoctrine()->getRepository(User::class)->findBy(
            [
                'username' => $user_name,'status' => 1
            ],
            ['id' => 'DESC']
        );

        if (empty($user)) {
            $this->addFlash("warning", "User Not Found");
            return $this->redirectToRoute('login');
        }

        // Check Password
        if (!password_verify($user_password, $user[0]->getPassword())) {
            $this->addFlash("warning", "Wrong Password");
            return $this->redirectToRoute('login');
        }
        // dd("Login Success");
        $role = $this->getDoctrine()
            ->getRepository(Role::class)
            ->findOneBy(['id' => $user[0]->getRoleId()]);

        $isClassTeacher = 0; // 0 = no , 1 = Yes

        if ($user[0]->getRoleId() == 2) {
            $userInfo = $this->getDoctrine()->getRepository(Admin::class)->findOneBy([
                'id' => $user[0]->getConcernPersonId()
            ]);

        }else {
            $this->addFlash("warning", "System Can't detect you. inform Support team.");
            return $this->redirectToRoute('login');
        }

        if (empty($userInfo)) {
            $this->addFlash("warning", "Please Try again");
            return $this->redirectToRoute('login');
        }


        $session->set('UserIdName', $user[0]->getUserName());
        $session->set('UserType', $user[0]->getRoleId());
        $session->set('UserRoleName', $role->getName());
        $session->set('UserConcernPerson', $user[0]->getConcernPersonId());
        $session->set('UserName', $userInfo->getName());
        $session->set('userID', $user[0]->getId());
        $session->set('userName', $user[0]->getFirstName());
        $session->set('userEmail', $user[0]->getEmail());
        $session->set('ContactNo', $userInfo->getContactNo());
        $session->set('userPhoto', $userInfo->getPhoto());

        return $this->redirectToRoute('orders');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function indexLogOut(SessionInterface $session)
    {
        $session->clear();
        return $this->redirectToRoute('login');
    }

    /**
     * @Route("/data", name="data")
     */
    public function data(UserPasswordEncoderInterface $passwordEncoder, SessionInterface $session)
    {

        return $this->render('class/index_new.html.twig', [
            'controller_name' => 'Template',
            'schools' => "jjj"
        ]);

        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $conn = $this->getDoctrine()->getConnection();
        $entityManager = $this->getDoctrine()->getManager();

        exit(dump("add data"));
    }


    ///// Email Check /////////

    /**
     * @Route("/email", name="email")
     */

    public function sendEmail(MailerInterface $mailer)
    {
        $data = "i am ";
        $email = (new Email())
            ->from('y4tube4@gmail.com')
            ->to('isheiblu@gmail.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration! ' . $data . ' </p>');

        $mailer->send($email);

        // ...
        dd("hi");
    }

    /**
     * @Route("/user/check-current-pass/{pass}")
     */
    public function check_current_pass(SessionInterface $session, LoggerInterface $logger, Request $request, $pass)
    {

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy([
                'school_id' => $session->get('userID'),
                'username' => $session->get('UserIdName')
            ]);

        // Check Password
        if (!password_verify($pass, $user->getPassword())) {
            $response = 'notexists';
        }else{
            $response = "exists";
        }

        return new Response(
            $response
        );

    }

    /**
     * @route("/changeLocale", name="changeLocale")
     */
    public function changeLocale(Request $request, SessionInterface $session)
    {
        $form = $this->createFormBuilder(null)
            ->add('locale', ChoiceType::class, [
                'choices' => [
                    'Bangla' 	=> 'bn',
                    'English'	=> 'en'
                ]
            ])
            ->add('save', SubmitType::class)
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $locale = $form->getData()['locale'];
            //dd($locale);
            //$user = $this->getUser();

            $user = $em
                ->getRepository(User::class)
                ->find($session->get('userID'));

            //dd($user);
            $user->setLocale($locale);
            $em->persist($user);
            $em->flush();

            $session->set('locale', $locale);

        }

        return $this->render('user/locale.html.twig', [
            'form'		=> $form->createView()
        ]);
    }
}
