<?php

namespace App\Controller;

use App\Entity\Orders;
use App\Entity\Door;
use App\Entity\User;
use App\Form\RoutingFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class OrdersController extends AbstractController
{


    /**
     * @Route("/orders", name="orders")
     */
    public function index(SessionInterface $session)
    {

        $orders = $this->getDoctrine()->getRepository(Orders::class)->findBy(
            [],
            []
        );

        //dd($orders);


        $doors = $this->getDoctrine()->getRepository(Door::class)->findBy(
            [],
            []
        );

        return $this->render('orders/index.html.twig', [
            'controller_name' => 'Order List',
            'orders' => $orders,
            'doors' => $doors
        ]);
    }

    /**
     * @Route("/order_create", name="order_create")
     */
    public function index_order_create(SessionInterface $session)
    {
        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        return $this->render('order/create.html.twig', [
            'controller_name' => 'Add Order'
        ]);
    }

    /**
     * @Route("/create_order", name="create_order", methods="POST")
     */
    public function create(Request $request, SessionInterface $session)
    {
        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $id = trim($request->request->get('id'));
        $status = trim($request->request->get('status'));

        if (empty($status)) {
            $this->addFlash("warning", "Please Give Status");
            return $this->redirectToRoute('order_create');
        } else {
            $entityManager = $this->getDoctrine()->getManager();
            if(!empty($id)){
                $order = $entityManager
                    ->getRepository(Orders::class)
                    ->find($id);


                $msg = 'Update';
            }else{
                $order = new Orders();
                $user = $entityManager->getRepository(User::class)->find($session->get('userID'));
                $order->setUser($user);
                $msg = 'Created';
            }


            $door = $entityManager->getRepository(Door::class)->find($request->request->get('door_id'));
            $order->setDoor($door);

            $order->setOrderStatus($status);
            $entityManager->persist($order);
            $entityManager->flush();

            $this->addFlash("success", "Order Successfully ". $msg);
            return $this->redirectToRoute('orders');
        }
    }

    /**
     * @Route("/order/edit_order")
     */
    public function editOrder(SessionInterface $session, LoggerInterface $logger, Request $request)
    {
        $id = $request->query->get('order_id');

        $entityManager = $this->getDoctrine()->getManager();

        $order = $entityManager
            ->getRepository(Orders::class)
            ->find($id);

        $data = [];

        $data['id'] = $id;
        $data['order_status'] = $order->getOrderStatus();

        $json = json_encode($data);

        return new JsonResponse($json);

    }

    /**
     * @Route("/order-delete/{id}", name="order_delete")
     */
    public function delete(SessionInterface $session, $id)
    {
        if ($session->get('userID') == null) {
            return $this->redirectToRoute('login');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $order = $entityManager->getRepository(Orders::class)->find($id);
        $entityManager->remove($order);
        $entityManager->flush();

        $this->addFlash("success", "Order Successfully Delete");
        return $this->redirectToRoute('orders');
    }

    /**
     * @Route("/order/check_order/{title}")
     */
    public function checkOrder(SessionInterface $session, LoggerInterface $logger, Request $request, $title)
    {
        $order = $this->getDoctrine()
            ->getRepository(Orders::class)
            ->findOneBy(['title' => $title]);

        if(!empty($order)){
            $response = "exists";

        }else{
            $response = 'notexists';
        }

        return new Response(
            $response
        );

    }

}