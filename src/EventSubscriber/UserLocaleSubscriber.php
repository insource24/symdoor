<?php
namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use Psr\Log\LoggerInterface;

/**
 * Stores the locale of the user in the session after the
 * login. This can be used by the LocaleSubscriber afterwards.
 */
class UserLocaleSubscriber implements EventSubscriberInterface
{
    private $requestStack;

    public function __construct(RequestStack $requestStack, LoggerInterface $logger)
    {
        $this->requestStack = $requestStack;
        $this->logger = $logger;

        $this->logger->info('mmmmmmm');
    }

    public function onLogin(InteractiveLoginEvent $event)
    {
        $this->logger->info('bbbbbb');


        $user = $event->getAuthenticationToken()->getUser();

        $this->logger->info('$user');
        $this->logger->info($user);

        if (null !== $user->getLocale()) {
            $this->requestStack->getSession()->set('_locale', $user->getLocale());
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => [
                ['onLogin', 16]
            ]
        ];
//        return [
//            SecurityEvents::INTERACTIVE_LOGIN => [['onInteractiveLogin', 20]],
//        ];
    }
}
