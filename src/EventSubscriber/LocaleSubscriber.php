<?php
namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Psr\Log\LoggerInterface;

class LocaleSubscriber implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct(string $defaultLocale = 'en', LoggerInterface $logger)
    {
        $this->defaultLocale = $defaultLocale;

        $this->logger = $logger;

        $this->logger->info('cccccc');
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        $this->logger->info('hhhhhhh');
        $this->logger->info($request->attributes->get('_locale'));
//        $this->logger->info($session->get('userID'));
        $this->logger->info($request->getSession()->get('locale'));

        // try to see if the locale has been set as a _locale routing parameter
        if (!empty($request->getSession()->get('locale'))) {
            $this->logger->info('ddddd');
            $this->logger->info($request->getSession()->get('locale'));
            $request->getSession()->set('_locale', $request->getSession()->get('locale'));

            $request->setLocale($request->getSession()->get('_locale', $request->getSession()->get('locale')));
        } else {
            $this->logger->info('eeeeeeee');
            $this->logger->info($this->defaultLocale);

            // if no explicit locale has been set on this request, use one from the session
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            // must be registered before (i.e. with a higher priority than) the default Locale listener
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
        ];
    }
}
